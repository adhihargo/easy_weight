import bpy
from bpy.props import *
from bpy.app.handlers import persistent

class EASYWEIGHT_OT_change_brush(bpy.types.Operator):
	"""Change the weight paint brush to a specific brush"""
	bl_idname = "brush.set_specific"
	bl_label = "Set WP Brush"
	bl_options = {'REGISTER', 'UNDO'}

	brush: EnumProperty(name="Brush",
	items=[('Add', 'Add', 'Add'),
			('Subtract', 'Subtract', 'Subtract'),
			('Draw', 'Draw', 'Draw'),
			('Average', 'Average', 'Average'),
			('Blur', 'Blur', 'Blur'),
			],
	default="Add")

	def execute(self, context):
		brush_name = self.brush
		brush = bpy.data.brushes.get(brush_name)
		if not brush:
			# Create the brush.
			brush = bpy.data.brushes.new(brush_name, mode='WEIGHT_PAINT')
			if brush_name == 'Add':
				brush.blend = 'ADD'
			if brush_name == 'Subtract':
				brush.blend = 'SUB'
			if brush_name == 'Blur':
				brush.weight_tool = 'BLUR'
			if brush_name == 'Average':
				brush.weight_tool = 'AVERAGE'
		
		# Configure brush.
		value = 0.5 if brush.falloff_shape == 'SPHERE' else 1.0	# We use a darker color to indicate when falloff shape is set to Sphere.
		if brush_name=='Add':
			brush.cursor_color_add = [value, 0.0, 0.0, 1.0] 
		if brush_name=='Subtract':
			brush.cursor_color_add = [0.0, 0.0, value, 1.0]
		if brush_name=='Blur':
			brush.cursor_color_add = [value, value, value, 1.0]

		# Set the brush as the active one.
		bpy.context.tool_settings.weight_paint.brush = brush

		return { 'FINISHED' }

@persistent
def register_brush_switch_hotkeys(dummy):
	# Without this, the hotkeys' properties get reset whenever the addon is disabled, which results in having to set the Add, Subtract, Blur brushes on the hotkeys manually every time.
	# However, with this, the hotkey cannot be changed, since this will forcibly re-create the original anyways.
	
	active_keyconfig = bpy.context.window_manager.keyconfigs.active
	if not active_keyconfig: return # Avoid error when running without UI.
	wp_hotkeys = active_keyconfig.keymaps['Weight Paint'].keymap_items

	add_hotkey = wp_hotkeys.new('brush.set_specific',value='PRESS',type='ONE',ctrl=False,alt=False,shift=False,oskey=False)
	add_hotkey.properties.brush = 'Add'
	add_hotkey.type = add_hotkey.type
	
	sub_hotkey = wp_hotkeys.new('brush.set_specific',value='PRESS',type='TWO',ctrl=False,alt=False,shift=False,oskey=False)
	sub_hotkey.properties.brush = 'Subtract'
	sub_hotkey.type = sub_hotkey.type
	
	blur_hotkey = wp_hotkeys.new('brush.set_specific',value='PRESS',type='THREE',ctrl=False,alt=False,shift=False,oskey=False)
	blur_hotkey.properties.brush = 'Blur'
	blur_hotkey.type = blur_hotkey.type

def register():
	from bpy.utils import register_class
	register_class(EASYWEIGHT_OT_change_brush)
	register_brush_switch_hotkeys(None)
	bpy.app.handlers.load_post.append(register_brush_switch_hotkeys)
	
def unregister():
	from bpy.utils import unregister_class
	unregister_class(EASYWEIGHT_OT_change_brush)
	bpy.app.handlers.load_post.remove(register_brush_switch_hotkeys)