# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

bl_info = {
	"name": "Easy Weight",
	"author": "Demeter Dzadik",
	"version": (1,0),
	"blender": (2, 90, 0),
	"location": "Weight Paint > Weights > Easy Weight",
	"description": "Operators to make weight painting easier.",
	"category": "Rigging",
	"doc_url": "https://gitlab.com/blender/easy_weight/-/blob/master/README.md",
	"tracker_url": "https://gitlab.com/blender/easy_weight/-/issues/new",
}

import bpy, importlib
from bpy.types import AddonPreferences
from bpy.props import BoolProperty

from . import smart_weight_transfer
from . import force_apply_mirror
from . import toggle_weight_paint
from . import change_brush
from . import weight_paint_context_menu
from . import vertex_group_operators
from . import vertex_group_menu
from . import rogue_weights

# Each module is expected to have a register() and unregister() function.
modules = [
	smart_weight_transfer,
	force_apply_mirror,
	toggle_weight_paint,
	change_brush,
	weight_paint_context_menu,
	vertex_group_operators,
	vertex_group_menu,
	rogue_weights
]

class EasyWeightPreferences(AddonPreferences):
	bl_idname = __name__

	debug_mode: BoolProperty(
		name='Debug UI',
		description='When enabled, add an EasyWeight panel to the Sidebar, used for debugging',
		default=False,
	)
	
	def draw(self, context):
		layout = self.layout

		layout.prop(self, 'debug_mode')

def register():
	from bpy.utils import register_class
	register_class(EasyWeightPreferences)
	for m in modules:
		importlib.reload(m)
		m.register()

def unregister():
	from bpy.utils import unregister_class
	unregister_class(EasyWeightPreferences)
	for m in modules:
		m.unregister()