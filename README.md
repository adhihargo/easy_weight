Easy Weight is an addon focused on quality of life improvements for weight painting in Blender.

### Brush Switching
The addon will force-register keybinds for this operator to the 1, 2, 3 keys in Weight Paint mode:  
1: Change to Add Brush.  
2: Change to Subtract Brush.  
3: Change to Blur Brush.  
The brushes must have their default name, ie. "Add", "Subtract", "Blur".  

### Entering Weight Paint Mode
The Toggle Weight Paint Mode operator lets you switch into weight paint mode easier.
Select your object and run the operator.  
- It will find the first armature modifier of your mesh, if there is one. It will ensure the armature is visible and in pose mode.  
- It will set the shading settings to a pure white.  
- If your object's display type was set to Wire, it will set it to Solid.
Run the operator again to restore everything to how it was before.  

I recommend setting up a keybind for this, eg.:  
<img src="docs/toggle_wp_shortcut.png" width="400" />  
If you don't want to use the hotkey editor, you can also just find the operator in the "Object" or "Weights" menus, and simply Right Click->Assign Shortcut.

### Weight Paint Context Menu
The default context menu (accessed via W key or Right Click) for weight paint mode is not very useful.
The Custom Weight Paint Context Menu operator is intended as a replacement for it.  
<img src="docs/custom_wp_context_menu.png" width="250" />  
_(The "OK" button is not needed, I just can't avoid it)_  

This panel provides quick access to commonly needed tools, whether they are part of core Blender or the addon:
- Global toggles for the Accumulate, Front Faces Only and Falloff Shape brush options.
- Weight Paint mode settings including a new "Clean Weights" option to run the Clean Weights operator after every brush stroke.
- Commonly used Overlay and Armature display settings.
- Commonly used or [new](#vertex-group-operators) operators.
It lets you change the brush falloff type (Sphere/Projected) and the Front Faces Only option. These will affect ALL brushes.  
Also, the color of your brushes will be darker when you're using Sphere falloff.  

I recommend to overwrite the shortcut of the default weight paint context menu like so:  
<img src="docs/wp_context_menu_shortcut.png" width="500" />  

### Hunting Rogue Weights
The addon provides a super sleek workflow for hunting down rogue weights efficiently but safely, with just the right amount of automation. This functionality can be found in the Sidebar->EasyWeight->Weight Islands panel.
<img src="docs/weight_islands.png" width="800" />  

- After pressing Calculate Weight Islands and waiting a few seconds, you will see a list of all vertex groups which consist of more than a single island. 
- Clicking the magnifying glass icon will focus the smallest island in the group, so you can decide what to do with it.
- If the island is rogue weights, you can subtract them and go back to the previous step. If not, you can press the checkmark icon next to the magnifying glass, and the vertex group will be hidden from the list.
- Continue with this process until all entries are gone from the list.
- In the end, you can be 100% sure that you have no rogue weights anywhere on your mesh.

### Vertex Group Operators
The Vertex Groups context menu is re-organized with more icons and better labels, as well as some additional operators:
<img src="docs/vg_context_menu.png" width="500" />  
- **Delete Empty Deform Groups**: Delete deforming groups that don't have any weights.  
- **Delete Unused Non-Deform Groups**: Delete non-deforming groups that aren't used anywhere, even if they do have weights.  
- **Delete Unselected Deform Groups**: Delete all deforming groups that don't correspond to a selected pose bone. Only in Weight Paint mode.  
- **Ensure Mirror Groups**: If your object has a Mirror modifier, this will create any missing vertex groups.  
- **Focus Deforming Bones**: Reveal and select all bones deforming this mesh. Only in Weight Paint mode.  
- **Focus Rogue Weights**: This real clever operator will cycle through vertex groups whose weights consist of more than a single island, then enter vertex masking mode and select the vertices of the smallest island. This still requires human eyes to check and make sure the islands are definitely not needed. You are meant to run this operator many times over and over to cycle through such vertex groups.  
TODO: Operator to ensure symmetrical weights.
If you have any more suggestions, feel free to open an Issue with a feature request.

### Force Apply Mirror Modifier
In Blender, you cannot apply a mirror modifier to meshes that have shape keys.  
This operator tries to anyways, by duplicating your mesh, flipping it on the X axis and merging into the original. It will also flip vertex groups, shape keys, shape key masks, and even (attempt) shape key drivers, assuming everything is named with .L/.R suffixes.  